# Order Management API (OM API)

## Endpoints

* Production `https://om-api.tamoco.com/`
* Staging `comming soon`

## Resources

* [Apps](./apps.md)
* [Inventory](./inventory.md)
