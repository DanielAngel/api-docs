# App

Apps refer to the mobile apps that have integrated our SDK. We need to keep a reference of them so we can track their engagement with us.

## List Apps

```
GET /v1/apps.json
```

**Params**

| Attribute | Description                       | Type              | Example | Required |
| --------- | -----------                       | ---               | ------- | ---      |
| page      | The page number, default 1        | `<Number>`        | `1`     | false    |
| per_page  | The records per page, default 25  | `<Number>`        | `30`    | false    |
| sort      | The column to sort by, id or name | `<String>`        | `id`    | false    |
| filter    | Filters                           | `<Array<String>>` | ``      | false    |

**Response Attributes**

| Attribute                 | Description                                                 | Type            | Example                                      |
| ---------                 | -----------                                                 | ---             | -------                                      |
| app["id"]                 | The app's ID                                                | `<Number>`      | `12`                                         |
| app["name"]               | The app's name                                              | `<String>`      | `"Acme iOS App"`                             |
| app["account_id"]         | The associated account                                      | `<String>`      | `"Acme"`                                     |
| app["device_os"]          | The platform the app runs on ios or android                 | `<String>`      | `"ios"`                                      |
| app["bundle_id"]          | The id of the App in the app store                          | `<String>`      | `"com.acme.acmeapp"`                         |
| app["private"]            | When private only the owner can target campaigns to the app | `<Boolean>`     | `true`                                       |
| app["sdk_secret"]         | The security key the app should use to authenticate with    | `<String>`      | `"52750b30ffbc7de3b36252750b30ffbc7de3b362"` |
| app["frequency_caps"]     | Frequency Caps define how often the same ad can be shown    | `<Array<Hash>>` | `[{},{}]`                                    |
| app["log_level"]          | The set log level                                           | `<String>`      | `"debug"`                                    |
| app["bug_fender_key"]     | The API key used to send log data to Bug Fender             | `<String>`      | `"52750b30ffbc7de3b36252750b30ffbc7de3b36"`  |
| app["inventory_interval"] | The interval in seconds between inventory pulls             | `<Number>`      | `3600`                                       |
| app["blocked"]            | Blocked apps are ignored by our event tracker               | `<Boolean>`     | `false`                                      |
| app["revision"]           | The version of the app                                      | `<Number>`      | `3`                                          |
| app["created_at"]         | The app's name                                              | `<Date>`        | `2016-06-15T13:04:15.430Z`                   |
| app["updated_at"]         | The app's name                                              | `<Date>`        | `2016-06-15T13:04:15.430Z`                   |

**Example**

```
curl -H "Content-Type: application/json" -H "Authorization: Bearer token" https://om-api.tamoco.com/v1/app.json
=> [{"id":179,"name":"iOS Tamoco Test App","account_id":2,"device_os":"ios","bundle_id":"com.tamoco.tamocotestapp","private":false,"sdk_secret":"100684bc46ce32c461914566112cf60225063777","frequency_caps":[],"log_level":"default","inventory_interval":3600,"bug_fender_key":"","blocked":false,"revision":3,"created_at":"2016-06-20T12:31:57.651Z","updated_at":"2016-12-13T11:48:43.587Z"},{"id":180,"name":"Android Tamoco Test App","account_id":2,"device_os":"android","bundle_id":"co.tamo.proximity.app","private":false,"sdk_secret":"48218aea3e20eb42d7fca7070ddb2a43c2add3d3","frequency_caps":[],"log_level":"default","inventory_interval":3600,"bug_fender_key":"","blocked":false,"revision":6,"created_at":"2016-06-20T12:32:26.442Z","updated_at":"2017-02-28T10:03:06.369Z"}, ...]
```

## Get an App

```
GET /v1/apps/:id.json
```

**Response Attributes**

| Attribute          | Description                                                 | Type            | Example                                      |
| ---------          | -----------                                                 | ---             | -------                                      |
| id                 | The app's ID                                                | `<Number>`      | `12`                                         |
| name               | The app's name                                              | `<String>`      | `"Acme iOS App"`                             |
| account_id         | The associated account                                      | `<String>`      | `"Acme"`                                     |
| device_os          | The platform the app runs on ios or android                 | `<String>`      | `"ios"`                                      |
| bundle_id          | The id of the App in the app store                          | `<String>`      | `"com.acme.acmeapp"`                         |
| private            | When private only the owner can target campaigns to the app | `<Boolean>`     | `true`                                       |
| sdk_secret         | The security key the app should use to authenticate with    | `<String>`      | `"52750b30ffbc7de3b36252750b30ffbc7de3b362"` |
| frequency_caps     | Frequency Caps define how often the same ad can be shown    | `<Array<Hash>>` | `[{},{}]`                                    |
| log_level          | The set log level                                           | `<String>`      | `"debug"`                                    |
| bug_fender_key     | The API key used to send log data to Bug Fender             | `<String>`      | `"52750b30ffbc7de3b36252750b30ffbc7de3b36"`  |
| inventory_interval | The interval in seconds between inventory pulls             | `<Number>`      | `3600`                                       |
| blocked            | Blocked apps are ignored by our event tracker               | `<Boolean>`     | `false`                                      |
| revision           | The version of the app                                      | `<Number>`      | `3`                                          |
| created_at         | The app's name                                              | `<Date>`        | `2016-06-15T13:04:15.430Z`                   |
| updated_at         | The app's name                                              | `<Date>`        | `2016-06-15T13:04:15.430Z`                   |


**Example**

```
curl -H 'authorization: Bearer 40d3c128fb50b0e5544ec60ab9af305efad8348561d7a6003390d48836f1b4fc' 'https://om-api.tamoco.com/v1/apps/180.json?'
=> {"id":180,"name":"Android Tamoco Test App","account_id":2,"device_os":"android","bundle_id":"co.tamo.proximity.app","private":false,"sdk_secret":"48218aea3e20eb42d7fca7070ddb2a43c2add3d3","frequency_caps":[],"log_level":"default","inventory_interval":3600,"bug_fender_key":"","blocked":false,"revision":6,"created_at":"2016-06-20T12:32:26.442Z","updated_at":"2017-02-28T10:03:06.369Z"}
```


## Creating an App

```
POST /v1/apps.json
```

**Params**

| Attribute                 | Description                                                 | Type            | Example                                     | Required |
| ---------                 | -----------                                                 | ---             | -------                                     | ---      |
| app["name"]               | The app's name                                              | `<String>`      | `"Acme iOS App"`                            | true     |
| app["device_os"]          | The platform the app runs on ios or android                 | `<String>`      | `"ios"`                                     | true     |
| app["bundle_id"]          | The id of the App in the app store                          | `<String>`      | `"com.acme.acmeapp"`                        | true     |
| app["frequency_caps"]     | Frequency Caps define how often the same ad can be shown    | `<Array<Hash>>` | `[{},{}]`                                   | true     |
| app["private"]            | When private only the owner can target campaigns to the app | `<Boolean>`     | `true`                                      | false    |
| app["log_level"]          | The set log level                                           | `<String>`      | `"debug"`                                   | false    |
| app["bug_fender_key"]     | The API key used to send log data to Bug Fender             | `<String>`      | `"52750b30ffbc7de3b36252750b30ffbc7de3b36"` | false    |
| app["inventory_interval"] | The interval in seconds between inventory pulls             | `<Number>`      | `3600`                                      | false    |


**Example**

```
curl -H "Content-Type: application/json" -H "Authorization: Bearer token" -X POST -d '{"app": {"name":"app1", "device_os":"android", "bundle_id":"com.acme.acmeapp","frequency_caps":[]}}' https://om-api.tamoco.com/v1/app.json
```

## Updating an App

```
PUT /v1/apps/:id.json
```

**Params**

| Attribute                 | Description                                                 | Type            | Example                                     | Required |
| ---------                 | -----------                                                 | ---             | -------                                     | ---      |
| app["name"]               | The app's name                                              | `<String>`      | `"Acme iOS App"`                            | false    |
| app["device_os"]          | The platform the app runs on ios or android                 | `<String>`      | `"ios"`                                     | false    |
| app["bundle_id"]          | The id of the App in the app store                          | `<String>`      | `"com.acme.acmeapp"`                        | false    |
| app["frequency_caps"]     | Frequency Caps define how often the same ad can be shown    | `<Array<Hash>>` | `[{},{}]`                                   | false    |
| app["private"]            | When private only the owner can target campaigns to the app | `<Boolean>`     | `true`                                      | false    |
| app["log_level"]          | The set log level                                           | `<String>`      | `"debug"`                                   | false    |
| app["bug_fender_key"]     | The API key used to send log data to Bug Fender             | `<String>`      | `"52750b30ffbc7de3b36252750b30ffbc7de3b36"` | false    |
| app["inventory_interval"] | The interval in seconds between inventory pulls             | `<Number>`      | `3600`                                      | false    |
| app["revision"]           | The version of the app                                      | `<Number>`      | `3`                                         | true     |

**Example**

```
curl -H "Content-Type: application/json" -H "Authorization: Bearer token" -X PUT -d '{"app": {"name":"app1", "device_os":"android", "bundle_id":"com.acme.acmeapp","frequency_caps":[]}}' https://om-api.tamoco.com/v1/app/180.json
```

## Fetching an Apps stats

You may want to see how a specific app is performing. To do this you can use the [RPT API](../rptapi/README.md). A few examples would be:

**Recent Daily performance**

```
curl -H 'authorization: Bearer 40d3c128fb50b0e5544ec60ab9af305efad8348561d7a6003390d48836f1b4fc' 'https://rpt-api.tamoco.com/v1/apps?account_id=2&filter%5Bapp_name=Android+Tamoco+Test+App&from=2017-6-21&grouping%5B%5D=date&order=date+DESC&per_page=50&to=2017-7-21'
=> [{"date":"2017-07-21","app_id":null,"bundle_id":null,"app_name":null,"os":null,"os_version":null,"device_type_id":null,"language":null,"order_id":null,"order_name":null,"line_item_id":null,"line_item_name":null,"ad_id":null,"creative_id":null,"creative_name":null,"inventory_id":null,"inventory_type":null,"inventory_name":null,"trigger_type":null,"country":null,"city":null,"tracked":165,"impressions":0,"clicks":0},...]
```
