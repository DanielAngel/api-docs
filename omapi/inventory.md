# Inventory

## Creating a piece of Inventory

```
POST /v1/inventories.json
```

We currently support multiple types of inventory.

* Beacons
* Geo Fence
* NFC
* QR Codes
* WIFI

Each inventory type has slightly different requirements.

### Common Parmas

These attributes apply to all the different types of Inventory.

| Attribute      | Description                                                                      | Type      | Example                                | Required |
| -------------- | -----------                                                                      | --------  | -------------------------------------- | -------- |
| name           | The name of the Inventory                                                        | `String`  | `"Beacon one"`                         | true     |
| latitude       | The latitude of the Inventory                                                    | `Number`  | `51.526695`                            | true     |
| longitude      | The longitude of the Inventory                                                   | `Number`  | `-0.086137`                            | true     |
| private        | Is app hidden from network.                                                      | `boolean` | `false`                                | true     |
| timezone       | The time of the Inventory                                                        | `String`  | `"Europe/London"`                      | true     |
| dwell_interval | This is the amount of time (in seconds) before a Dwell based Event is triggered. | `Number`  | `55`                                   | false    |
| place_type_id  | ID of palce type associated with this Inventory                                  | `Number`  | `12`                                   | false    |
| external_id    | An arbitrary ID to associate to the Inventory                                    | `Number`  | `56891`                                | false    |

### Beacons Parmas

Beacons are pieces of hardware which are in fixed locations. The have a UUID, Major and Minor values. When creating a beacon in the system the following attributes are supported:


| Attribute      | Description                                                                      | Type      | Example                                | Required |
| -------------- | -----------                                                                      | --------  | -------------------------------------- | -------- |
| kind           | Inventory kind                                                                   | `String`  | `"beacon"`                             | true     |
| uuid           | The Beacon's Universally Unique Identifier.                                      | `String`  | `f7826da6-4fa2-4e98-8024-bc5b71e0893e` | true     |
| major          | The major value of the Beacon.                                                   | `Number`  | `32146`                                | true     |
| minor          | The minor value of the Beacon.                                                   | `Number`  | `12346`                                | true     |
| hover_interval | This is the amount of time (in seconds) before a Hover based Event is triggered. | `Number`  | `55`                                   | false    |

### Geo Fences Parmas

Geo Fences are plotted areas on a map. When creating a Geo Fence in the system the following attributes are supported:

| Attribute      | Description                                               | Type     | Example           | Required |
| -------------- | -----------                                               | -------- | ----------------- | -------- |
| kind           | Inventory kind                                            | `String` | `"geo_fence"`     | true     |
| radius         | The radius (in meters) around the coordinates to monitor. | `Number` | `350`             | true     |

### NFC Parmas

NFC Tags are electronic chips which store a redirect URL. When a phone scans them they are forwarded to the URL. When creating a NFC tag in the system the following attributes are supported:

| Attribute       | Description             | Type     | Example                         | Required |
| --------------- | --------                | -------- | ------------------------------- | -------- |
| kind            | Inventory kind          | `String` | `"nfc"`                         | true     |
| destination_url | Default destination url | `String` | `"http://someredirect.url.com"` | true     |

### QR Codes Parmas

QR Codes are matrix barcodes which when scanned redirect the user to a URL. When creating a QR Code in the system the following attributes are supported:

| Attribute       | Description             | Type     | Example                         | Required |
| --------------- | --------                | -------- | ------------------------------- | -------- |
| kind            | Inventory kind          | `String` | `"qr"`                          | true     |
| destination_url | Default destination url | `String` | `"http://someredirect.url.com"` | true     |

### WIFI Parmas

WIFI inventory are wifi routers found in fixed locations. When creating a WIFI in the system the following attributes are supported:

| Attribute      | Description                | Type     | Example             | Required |
| -------------- | -----------                | -------- | ------------------- | -------- |
| kind           | Inventory kind             | `String` | `"wifi"`            | true     |
| ssid           | The Service Set Identifier | `String` | `"coffeshoprouter"` | true     |

**Example Requests**

```
curl -H "Authorization: TOCOME" -H "Content-Type: application/json" -X POST -d '{"name":"Beacon 1","kind":"beacon","latitude":"53.9600853","longitude":"-1.0841745","dwell_interval":55,"external_id":456789,"hover_interval":55,"major":"32146","minor":"12346","private":true,"time_zone":"Europe/London","uuid":"f7826da6-4fa2-4e98-8024-bc5b71e0893e"}' "http://om-api.tamoco.com/v1/inventories?account_id=1"
```

### FAQ

**My Inventory was rejected by the systems, location wasn't found?**

When creating a  piece of Inventory our systems looks up the location based on the given coordinates. If that location can not be found then we may raise an error. We're taking more steps to resolve locations so this shouldn't happen moving forward.
