# Inventory Management API (INV API)

The inventory management API allows user to maintain Inventory. This application replaces the Inventory section of OM API.

## Endpoints

* Production `https://inv-api.tamoco.com/`
* Staging `comming soon`

## Resources

* [Inventory](./inventory.md)
