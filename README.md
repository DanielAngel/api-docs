# Tamoco API Documentation

This documentation combines all the API descriptions from our 3 tools. SSO, OM API and RPT API.

## Resources

* [Single Sign On (SSO)](./sso/README.md)
* [Order Management API (OM API)](./omapi/README.md)
* [Reporting API (RPT API)](./rptapi/README.md)
* [Events (EVT)](./evt/README.md)
* [Inventory API (INV API)](./invapi/README.md)
* [Places API (PLC API)](./plcapi/README.md)
