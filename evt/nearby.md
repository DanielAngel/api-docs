# Nearby

The SDK must perform [NearbyRequest] calls to the API in regular intervals (configurable, default: 1h).

* Request: `POST /v1/nearby`
* Example: [nearby-request.json]

The API then responds with a [NearbyResponse], returning a list of nearby inventory, such as known [inventory types].

* Example: [nearby-response.json]

[inventory types]: https://gist.github.com/dim/10035960a1e68600579b0e5a3ad03993#file-events-proto-L48
[NearbyRequest]: https://gist.github.com/dim/10035960a1e68600579b0e5a3ad03993#file-events-proto-L161
[NearbyResponse]: https://gist.github.com/dim/10035960a1e68600579b0e5a3ad03993#file-events-proto-L171
[nearby-request.json]: https://gist.github.com/dim/10035960a1e68600579b0e5a3ad03993#file-nearby-request-json
[nearby-response.json]: https://gist.github.com/dim/10035960a1e68600579b0e5a3ad03993#file-nearby-response-json
