# Track Request

```
{
  "event_id": "4763cae5-2275-cae1-f05e-afe98ad8c449",
  "sdk_timestamp": 1414141414,
  "device": {
    "device_id": "41c61b0f-8430-4e61-b178-20a72b53247b",
    "ip": "1.2.3.4",
    "device_type": 1,
    "make": "Apple",
    "model": "iPhone",
    "locale": "en_GB",
    "os": "IOS",
    "os_version": "7.1.1",
    "hw_version": "6S"
  },
  "app": {
    "app_id": "com.rovio.angrybirds",
    "app_name": "Angry Birds",
    "app_version": "2.3.7",
    "sdk_version": "1.2.8"
  },
  "geo": {
    "latitude": 51.524696,
    "longitude": -0.085004,
    "accuracy": 0.8
  },
  "trigger": {
    "trigger_type": 11,
    "inventory_id": 127,
    "beacon_id": "e1c27074-aae9-49fa-a6f1-bc715702bdb0",
    "major": 2,
    "minor": 1,
    "proximity": 6.8
  }
}
```

# Track Response

### Snippet

```
{
  "creative_type": 1,
  "notification": {
    "body": "Dinner at Mario's, 2-for-1 offer!"
  },
  "snippet": "<html><body><script>!function(t,x,o,n,e,i,a){"use strict";var r,c,d=Date.now()/1e3|0,u="xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g,function(t){var x=16*o()|0,n="x"===t?x:3&x|8;return n.toString(16)});r=function(x,o){t.location.href="https://evt.tamoco.com/v1/redirect?event_id="+u+"&timestamp="+d+"&code="+n+"&variant="+e+"&tac="+i+"&tagID="+a+(o||"")},c=function(t){r("","&latitude="+t.coords.latitude+"&longitude="+t.coords.longitude)},x.geolocation?x.geolocation.getCurrentPosition(c,r):r()}(window,navigator,Math.random,"%%CODE%%","%%VARIANT%%","%%TAC%%","%%TAGID%%");
    4: </script></body></html>",
  "tracked": "CtsBCsEBChBHY8rlInXK4fBer+mK2MRJEOarqKIFGkAKEEHGGw+EME5hsXggpytTJHsaBAECAwQgASoFQXBwbGUyBmlQaG9uZToFZW5fR0JCA0lPU0oFNy4xLjFSAjZTIg8NShlOQhWUFq69Hc3MTD8qIQgLEhgIfxoQ4cJwdKrpSfqm8bxxVwK9sCACKAEdmpnZQDIxChRjb20ucm92aW8uYW5ncnliaXJkcxILQW5ncnkgQmlyZHMaBTIuMy43IgUxLjIuOBDnq6iiBRg/GEAiA0dCUioGTG9uZG9uEg0I2V4Q1wIY1AEgBSgC"
}
```

### Redirect

```
{
  "creative_type": 2,
  "notification": {
    "body": "Dinner at Mario's, 2-for-1 offer!"
  },
  "redirect": "https://example.com/page?view=voucher&partner=23&reference=ax",
  "tracked": "CtsBCsEBChBHY8rlInXK4fBer+mK2MRJEOarqKIFGkAKEEHGGw+EME5hsXggpytTJHsaBAECAwQgASoFQXBwbGUyBmlQaG9uZToFZW5fR0JCA0lPU0oFNy4xLjFSAjZTIg8NShlOQhWUFq69Hc3MTD8qIQgLEhgIfxoQ4cJwdKrpSfqm8bxxVwK9sCACKAEdmpnZQDIxChRjb20ucm92aW8uYW5ncnliaXJkcxILQW5ncnkgQmlyZHMaBTIuMy43IgUxLjIuOBDnq6iiBRg/GEAiA0dCUioGTG9uZG9uEg0I2V4Q1wIY1AEgBSgC"
}
```

### JSON

```
{
  "creative_type": 3,
  "notification": {
    "body": "Dinner at Mario's, 2-for-1 offer!"
  },
  "snippet": "{\"key\":\"value\"}",
  "tracked": "CtsBCsEBChBHY8rlInXK4fBer+mK2MRJEOarqKIFGkAKEEHGGw+EME5hsXggpytTJHsaBAECAwQgASoFQXBwbGUyBmlQaG9uZToFZW5fR0JCA0lPU0oFNy4xLjFSAjZTIg8NShlOQhWUFq69Hc3MTD8qIQgLEhgIfxoQ4cJwdKrpSfqm8bxxVwK9sCACKAEdmpnZQDIxChRjb20ucm92aW8uYW5ncnliaXJkcxILQW5ncnkgQmlyZHMaBTIuMy43IgUxLjIuOBDnq6iiBRg/GEAiA0dCUioGTG9uZG9uEg0I2V4Q1wIY1AEgBSgC"
}
```

# Click Request

```
{
  "source": "CtsBCsEBChBHY8rlInXK4fBer+mK2MRJEOarqKIFGkAKEEHGGw+EME5hsXggpytTJHsaBAECAwQgASoFQXBwbGUyBmlQaG9uZToFZW5fR0JCA0lPU0oFNy4xLjFSAjZTIg8NShlOQhWUFq69Hc3MTD8qIQgLEhgIfxoQ4cJwdKrpSfqm8bxxVwK9sCACKAEdmpnZQDIxChRjb20ucm92aW8uYW5ncnliaXJkcxILQW5ncnkgQmlyZHMaBTIuMy43IgUxLjIuOBDnq6iiBRg/GEAiA0dCUioGTG9uZG9uEg0I2V4Q1wIY1AEgBSgC"
}
```
