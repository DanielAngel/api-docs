# Places

** Type Reference **

The current Place types.

| Type ID | Label             |
| ---     | ---               |
| 0       | UNKNOWN_TYPE      |
| 1       | BANK              |
| 2       | CAFE              |
| 3       | CONVENIENCE_STORE |
| 4       | FOOD              |
| 5       | PHARMACY          |
| 6       | STORE             |


## List Places

```
GET /v1/places?account_id=:account_id
```

**Params**

| Attribute  | Description                       | Type              | Example | Required |
| ---------  | -----------                       | ---               | ------- | ---      |
| account_id | The associated Account ID         | `<Number>`        | `1`     | true     |
| page       | The page number, default 1        | `<Number>`        | `1`     | false    |
| per_page   | The records per page, default 25  | `<Number>`        | `30`    | false    |
| sort       | The column to sort by, id or name | `<String>`        | `id`    | false    |
| filter     | Filters                           | `<Array<String>>` |         | false    |


**Response Attributes**

| Attribute                        | Description                                         | Type              | Example                   |
| ---------                        | -----------                                         | ---               | -------                   |
| plc["id"]                        | The unique ID                                       | `<Number>`        | `279657`                  |
| plc["latitude"]                  | The Place's Latitude                                | `<Number>`        | `51.5215`                 |
| plc["longitude"]                 | The Place's Longitude                               | `<Number>`        | `-0.0755`                 |
| plc["chain"]                     | The Places chain                                    | `<Hash>`          | `{}`                      |
| plc["chain"\]["id"]              | The Chain ID                                        | `<Number>`        | `666`                     |
| plc["chain"\]["name"]            | The Chain name                                      | `<String>`        | `"Costa Coffee"`          |
| plc["address"]                   | The Place's Address                                 | `<Hash>`          | `{}`                      |
| plc["address"\]["country"]       | The Address Country                                 | `<String>`        | `"GB"`                    |
| plc["address"\]["city"]          | The Address City                                    | `<String>`        | `"London"`                |
| plc["address"\]["postal_code"]   | The Address Postal Code                             | `<String>`        | `"E1 6AZ"`                |
| plc["address"\]["street_address"]| The Address Street Address                          | `<String>`        | `"132 Commercial Street"` |
| plc["opening_hours"]             | The Place's Opening Hour                            | `<Array<Hash>>`   | `[{}]`                    |
| plc["opening_hours"\]["opening"]  | The Time the Place opens                            | `<Number>`        | `510`                     |
| plc["opening_hours"\]["closing"]  | The Time the Place Closes                           | `<Number>`        | `1080`                    |
| plc["types"]                     | The Place Type, See types reference.                | `<Array<Number>>` | `[2.4]`                   |
| plc["radius"]                    | The Radius around the place which indicates a visit | `<Number>`        | `25`                      |

**Header Response**

| Attribute     | Description                                         | Type       | Example |
| ---------     | -----------                                         | ---        | ------- |
| X-Page        | The current Page                                    | `<Number>` | `1`     |
| X-Per-Page    | The records Per Page                                | `<Number>` | `2`     |
| X-Total-Count | The total number of records which match the request | `<Number>` | `10771` |


**Example**

```
curl -si 'https://plc-api.tamoco.com/v1/places?page=1&per_page=2&account_id=10' -H 'authorization: Bearer 16a8dd583f012c71d25951deabb5205ab941b712d73ee6b6875bbd342faf0e82'
=>
HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8
X-Page: 1
X-Per-Page: 2
X-Total-Count: 10771

[]
```


## Get Place

```
GET /v1/place/:id?account_id=:account_id
```

**Params**

| Attribute  | Description               | Type       | Example | Required |
| ---------  | -----------               | ---        | ------- | ---      |
| account_id | The associated Account ID | `<Number>` | `1`     | true     |
| id         | The Place ID          | `<Number>` | `1`     | true     |


**Response Attributes**

| Attribute                 | Description                                         | Type              | Example                   |
| ---------                 | -----------                                         | ---               | -------                   |
| id                        | The unique ID                                       | `<Number>`        | `279657`                  |
| latitude                  | The Place's Latitude                                | `<Number>`        | `51.5215`                 |
| longitude                 | The Place's Longitude                               | `<Number>`        | `-0.0755`                 |
| chain                     | The Places chain                                    | `<Hash>`          | `{}`                      |
| chain["id"]               | The Chain ID                                        | `<Number>`        | `666`                     |
| chain["name"]             | The Chain name                                      | `<String>`        | `"Costa Coffee"`          |
| address                   | The Place's Address                                 | `<Hash>`          | `{}`                      |
| address["country"]        | The Address Country                                 | `<String>`        | `"GB"                     |
| address["city"]           | The Address City                                    | `<String>`        | `"London"`                |
| address["postal_code"]    | The Address Postal Code                             | `<String>`        | `"E1 6AZ"`                |
| address["street_address"] | The Address Street Address                          | `<String>`        | `"132 Commercial Street"` |
| opening_hours             | The Place's Opening Hour                            | `<Array<Hash>>`   | `[{}]`                    |
| opening_hours["opening"]  | The Time the Place opens                            | `<Number>`        | `510`                     |
| opening_hours["closing"]  | The Time the Place Closes                           | `<Number>`        | `1080`                    |
| types                     | The Place Type, See types reference.                | `<Array<Number>>` | `[2.4]`                   |
| radius                    | The Radius around the place which indicates a visit | `<Number>`        | `25`                      |


```
curl -si 'https://plc-api.tamoco.com/v1/places/27598?page=1&per_page=2&account_id=10' -H 'authorization: Bearer 16a8dd583f012c71d25951deabb5205ab941b712d73ee6b6875bbd342faf0e82'
=>
HTTP/1.1 200 OK
Content-Type: application/json; charset=UTF-8
X-Page: 1
X-Per-Page: 2
X-Total-Count: 10771

{}
```
