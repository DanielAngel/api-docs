# Place Management API (PLC API)

The Place Management API allows user to maintain Places.

## Endpoints

* Production `https://plc-api.tamoco.com/`
* Staging `comming soon`

## Resources

* [Places](./places.md)
