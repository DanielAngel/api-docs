# Campaigns Stats

## Get App Stats

```
GET /v1/inventory.json
```

**Params**

| Attribute                  | Description                                                            | Type               | Example        | Required |
| ---------                  | -----------                                                            | ----               | -------        | -----    |
| account_id                 | The account id                                                         | `Integer`          | `1`            | true     |
| from                       | The from date                                                          | `<Date>`           | `"2017-01-01"` | true     |
| to                         | The to date                                                            | `<Date>`           | `"2017-01-31"` | true     |
| filter["ad_id"][]          | "The Ad to filter the results on"                                      | `<Array<Integer>>` | `[""]`         | false    |
| filter["app_id"][]         | "The Apps to filter the results on"                                    | `<Array<Integer>>` | `[""]`         | false    |
| filter["app_name"][]       | "The App name to filter the results on"                                | `<Array<String>>`  | `[""]`         | false    |
| filter["bundle_id"][]      | "The Bundle ID to filter the results on"                               | `<Array<String>>`  | `[""]`         | false    |
| filter["city"][]           | "The City                                                              | `<Array<String>>`  | `[""]`         | false    |
| filter["country"][]        | "The Country Code"                                                     | `<Array<String>>`  | `[""]`         | false    |
| filter["creative_id"][]    | "The Creative to filter the results on"                                | `<Array<Integer>>` | `[""]`         | false    |
| filter["device_type_id"][] | "The Device Type IDs to filter the results on",                        | `<Array<Integer>>` | `[""]`         | false    |
| filter["inventory_id"][]   | "The Inventory to filter the results on"                               | `<Array<Integer>>` | `[""]`         | false    |
| filter["inventory_name"][] | "The Inventory name to filter the results on"                          | `<Array<String>>`  | `[""]`         | false    |
| filter["inventory_type"][] | "The Inventory Type to filter the results on"                          | `<Arry<String>>`   | `[""]`         | false    |
| filter["language"][]       | "The Languages to include"                                             | `<Array<String>>`  | `[""]`         | false    |
| filter["line_item_id"][]   | "The Line Item to filter the results on"                               | `<Array<Integer>>` | `[""]`         | false    |
| filter["order_id"][]       | "The Order to filter the results on"                                   | `<Array<Integer>>` | `[""]`         | false    |
| filter["os"][]             | "The OSs to filter the results on",                                    | `<Array<String>>`  | `["ios"]`      | false    |
| filter["os_version"][]     | "The OS Versions to filter the results on"                             | `<Array<String>>`  | `[""]`         | false    |
| filter["source_id"][]      | "The Sources to filter the results on"                                 | `<Array<Integer>>` | `[""]`         | false    |
| filter["trigger_type"][]   | "The Triggers to filter the results on"                                | `<Array<Integer>>` | `[""]`         | false    |
| page                       | 'The page number. Default `1`'                                         | `<Integer>`        | `2`            | false    |
| per_page                   | The amount of records to return per page. Default `100` maximum `1000` | `<Integer>`        | `30`           | false    |
| grouping                   | The columns to Group By                                                | `<Array<String>`   | `["date"]`     | false    |
| order                      | The order to return the data. Default `events DESC`                    | `<String>`         | `"date DESC"`  | false    |

**Example**

Standard Call

```
curl 'https://rpt-api.tamoco.com/v1/inventory?account_id=85&from=2017-7-12&grouping%5B%5D=date&order=date+DESC&per_page=50&to=2017-7-19'  -H 'authorization: Bearer c87a13354da86918e540e5bafd5de3dd3dab5f1caecfe2aa2cc33f47435ba453'
```

With Filters:

Filter can be sent as arrays:

```
?filter[inventory_id][]=111&filter[inventory_id][]=222
```

e.g.

```
curl 'https://rpt-api.tamoco.com/v1/inventory?account_id=85&from=2017-7-12&grouping%5B%5D=date&order=date+DESC&per_page=50&to=2017-7-19&filter%5Binventory_id%5D%5B%5D=111&filter%5Binventory_id%5D%5B%5D=222'  -H 'authorization: Bearer c87a13354da86918e540e5bafd5de3dd3dab5f1caecfe2aa2cc33f47435ba453'
```
