# Reports API (RPT API)

The Reports API allows you to retrieve stats for a given Account. Apps, Inventory and Campaigns are all scoped to an Account so you can pull stats for each of these resources. Each resource has multiple filters and group options in which pivot data.

## Endpoints

* Production `https://rpt-api.tamoco.com/`
* Staging `comming soon`

## Resources

* [Apps](./apps.md)
* [Campaigns](./campaigns.md)
* [Inventory](./inventory.md)
